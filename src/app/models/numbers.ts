export interface Numbers {
    counter: number;
    number: number;
    quantity: number;
    first: number;
    last: number;
    color: string;
}