export interface Answer {
    data?: any;
    error?: any;
    success: boolean;
  }