export interface Texts {
    paragraph?: string;
    number?: number;
    hasCopyright?: boolean;
}